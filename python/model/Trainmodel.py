import tensorflow as tf
import numpy as np


from emnist import extract_training_samples
from emnist import extract_test_samples

train_images, train_labels = extract_training_samples("letters")
test_images, test_labels = extract_test_samples("letters")

train_images = np.array(train_images) / 255.0
train_labels = np.array(train_labels)
test_images = np.array(test_images) / 255.0
test_labels = np.array(test_labels)

##################################################
#Reshaping  --> not needed
#The data provided by EMNIST-python is allready shaped correctly
##################################################
#
#train_images = train_images.reshape(train_images.shape[0], 28, 28)
#test_images = test_images.reshape(test_images.shape[0], 28, 28)
#
#does not alter the data
#print(train_images.shape)
#print(test_images.shape)
#
#train_images = train_images.reshape(train_images.shape[0], 28, 28)
#test_images = test_images.reshape(test_images.shape[0], 28, 28)
#
#print(train_images.shape)
#print(test_images.shape)

##################################################
#Image preprocessing --> not needed
#The data provided by EMNIST-python is allready shaped correctly
##################################################

##################################################
#Creation of model
##################################################
#We use keras  
from keras.models import Sequential
from keras import optimizers
from keras.layers import Convolution2D, MaxPooling2D, Dropout, Flatten, Dense, Reshape, LSTM
from keras import backend as K
from keras.constraints import maxnorm
def resh(ipar):
    opar = []
    for image in ipar:
        opar.append(image.reshape(-1))
    return np.asarray(opar)

from keras.utils import np_utils

train_images = train_images.astype('float32')
test_images = test_images.astype('float32')

train_images = resh(train_images)
test_images = resh(test_images)

train_labels = np_utils.to_categorical(train_labels, 62)
test_labels = np_utils.to_categorical(test_labels, 62)

K.set_learning_phase(1)

model = Sequential()
model.add(Reshape((28,28,1), input_shape=(784,)))
model.add(Convolution2D(32, (5,5), input_shape=(28,28,1),
                            activation='relu',padding='same',
                            kernel_constraint=maxnorm(3)))
model.add(Convolution2D(32, (5,5),activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Flatten())
model.add(Dense(512, activation='relu', kernel_constraint=maxnorm(3)))
model.add(Dropout(0.5))
model.add(Dense(62, activation='softmax'))
opt = optimizers.Adamax(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)
model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

##################################################
#Training of model and evaluation
##################################################
history = model.fit(train_images,train_labels,validation_data=(test_images, test_labels), batch_size=128, epochs=20)

##################################################
#Exporting the model
##################################################

from keras.models import load_model
from keras.models import model_from_json

model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
model.save_weights("model.h5")