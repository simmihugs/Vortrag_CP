import random
from sklearn.datasets import load_digits
from sklearn import ensemble

digits = load_digits()
images_and_labels = list(zip(digits.images, digits.target))

count = len(digits.images)
images = digits.images.reshape((count, -1))
labels = digits.target

image_index = random.sample(range(count) , count//5) 
test_index = [i for i in range(count) 
                if i not in image_index]

test_images = [images[i] for i in test_index]
test_labels = [labels[i] for i in test_index]

images = [images[i] for i in image_index]
labels = [labels[i] for i in image_index]


classifier = ensemble.RandomForestClassifier()
classifier.fit(images, labels)
guess = classifier.predict(
        test_images[random.choice(test_index)].reshape(1,-1))
