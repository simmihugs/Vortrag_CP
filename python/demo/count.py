def x_y(count):
    """
    finds smallest grid setting
    """
    def diff(a,b):
        return abs(a - b)
    dividors = []
    if count == 1:
        return 1,1
    else:
        for index in range(1, count):
            if count % index == 0:
                dividors.append((count / index, count /(count/index)))
        difference = [diff(i[0],i[1]) for i in dividors]
        value, index2 = min((value, index2) for (index2, value) in enumerate(difference))
        return int(dividors[index2][0]),int(dividors[index2][1])
