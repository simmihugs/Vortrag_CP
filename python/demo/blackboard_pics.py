import cv2
import numpy as np
import os

def invert_pictures_blackboard():
    """
    Picture inverter 
    """
    pictures = [i for i in os.listdir("blackboard//") if i.endswith("jpg") or i.endswith("png")] 
    for pic in pictures:

        #opencv loads the image in BGR, convert it to RGB
        img = cv2.cvtColor(cv2.imread("blackboard//" + pic),cv2.COLOR_BGR2RGB)
        lower_white = np.array([220, 220, 220], dtype=np.uint8)
        upper_white = np.array([255, 255, 255], dtype=np.uint8)
        mask = cv2.inRange(img, lower_white, upper_white)  

        #could also use threshold
        mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, 
                cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))  
        
        #"erase" the small white points in the resulting mask
        mask = cv2.bitwise_not(mask)  

        #load background (could be an image too)
        bk = np.full(img.shape, 255, dtype=np.uint8)  

        # get masked foreground
        fg_masked = cv2.bitwise_and(img, img, mask=mask)

        # get masked background, mask must be inverted 
        mask = cv2.bitwise_not(mask)
        bk_masked = cv2.bitwise_and(bk, bk, mask=mask)

        # combine masked foreground and masked background 
        final = cv2.bitwise_or(fg_masked, bk_masked)

        #revert mask to original
        mask = cv2.bitwise_not(mask)  
        cv2.imwrite("blackboard//outblack" + pic, bk_masked)
