import matplotlib.pyplot as plt
import string
import skimage.io as io
import os
import argparse
from keras.models import model_from_json
from skimage.transform import resize

#parser
#for setup of plot grid
parser = argparse.ArgumentParser()
parser.add_argument('-xp', '--x_phone', action='store', dest='xp',
                    help='Enter a x value for the phone grid')
parser.add_argument('-yp', '--y_phone', action='store', dest='yp',
                    help='Enter a y value for the phone grid')
parser.add_argument('-xb', '--x_blackboard', action='store', dest='xb',
                    help='Enter a x value for the blackboard grid')
parser.add_argument('-yb', '--y_blackboard', action='store', dest='yb',
                    help='Enter a y value for the blackboard grid')
values = parser.parse_args()

#calls picture preprocessing scripts
from phone_pics import invert_pictures_phone
from blackboard_pics import invert_pictures_blackboard
from count import x_y

invert_pictures_phone()
invert_pictures_blackboard()

#loads model
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights('model.h5')
model = loaded_model
alph = list(string.ascii_uppercase)

#creat lists to loop threw for prediction and plotting
pictures_blackboard = ["blackboard//" + i for i in os.listdir("blackboard//") if i.find("outb") != -1]
pictures_phone = ["phone//" + i for i in os.listdir("phone//") if i.find("outp") != -1]
#creat lists to loop threw for grid dimensions
Values = [(values.xp,values.yp),(values.xb,values.yb)]
#creat lists to loop threw for picture naming

pictures_names = [" "," "]
pictures_locations = []
if len(pictures_phone) != 0:
     pictures_locations.insert(0, pictures_phone)
     pictures_names[0] = "phone"
if len(pictures_blackboard) != 0:
     pictures_locations.insert(1, pictures_blackboard)
     if pictures_names[0] == "phone":
         pictures_names[1] = "blackboard"
     else:
        pictures_names[0] = "blackboard"


#predicts & creates plots
for index,value in enumerate(pictures_locations):
    try:
        #if x and y coordinates for the picture grid are entered
        #set those for the grid
        x = int(Values[index][0]) 
        y = int(Values[index][1])
    except TypeError:
        #if not inver those 
        x,y = x_y(len(value))

    for index2,img in enumerate(value):
        #load images
        use_image = io.imread(img)

        #rescale/resize images
        use_img = use_image[:,:,:1]
        use_img_scal = resize(use_img, (28,28))

        #predict images
        prediction = int(str(model.predict_classes(use_img_scal.reshape(1,784)))[1:-1])
        
        #plot prediction as title & images
        plt.subplot(y, x ,index2 + 1)
        plt.imshow(use_image)
        plt.title("Prediction: {}".format(str(alph[prediction - 1])))
        plt.axis("off")
    plt.subplots_adjust(hspace = 0.4, wspace = 0.4)
    #save plot as png
    plt.savefig("result_" + str(pictures_names[index]) + ".png")
    plt.show()
    #delete prepared images
    for img in value:
        os.remove(img)
