import matplotlib.pyplot as plt
import string
import skimage.io as io
import os
import argparse
from keras.models import model_from_json
from skimage.transform import resize

#parser
#for setup of plot grid
parser = argparse.ArgumentParser()
parser.add_argument('-x', '--x_value', action='store', dest='x',
                    help='Enter a x value for the grid')
parser.add_argument('-y', '--y_value', action='store', dest='y',
                    help='Enter a y value for the grid')
values = parser.parse_args()

#calls picture preprocessing scripts
from phone_pics import invert_pictures_phone
from blackboard_pics import invert_pictures_blackboard
invert_pictures_phone()
invert_pictures_blackboard()

#loads model
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights('model.h5')
model = loaded_model
alph = list(string.ascii_uppercase)

pictures_blackboard = ["blackboard//" + i for i in os.listdir("blackboard//") if i.find("outb") != -1]
pictures_phone = ["phone//" + i for i in os.listdir("phone//") if i.find("outp") != -1]

#Change x,y to maximum of plotgrid
try:
    x = int(values.x) 
except TypeError:
    x = 7
try:
    y = int(values.y)
except TypeError:
    y = 7

#predicts & creates plots
for i in [pictures_phone, pictures_blackboard]:
    for index,img in enumerate(i):
        anzahl = len(i)
        handy_image = io.imread(img)
        handy_img = handy_image[:,:,:1]
        handy_img_scal = resize(handy_img, (28,28))
        prediction = int(str(model.predict_classes(handy_img_scal.reshape(1,784)))[1:-1])
        plt.subplot(x, y ,index+1)
        plt.imshow(handy_image)
        plt.title("Prediction: {}".format(str(alph[prediction-1])))
    plt.show()
    for j in i:
        os.remove(j)
