import PIL.ImageOps    
import os
from PIL import Image

def invert_pictures_phone():
    """
    Inverts the pictures
    """
    pictures = [i for i in os.listdir("phone//") if i.endswith("png") or i.endswith("jpg")]
    for pic in (pictures):
        image = Image.open("phone//" + pic)
        image = image.convert('RGB')
        inverted_image = PIL.ImageOps.invert(image)
        inverted_image.save("phone//outphone" + pic)
